import { CreateCategoryDto } from 'src/category/dto/create-category.dto';

export class CreateSubcategoryDto extends CreateCategoryDto {}
